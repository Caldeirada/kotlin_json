package gui

import entities.ArrayElement
import entities.ObjectElement
import entities.ValueElement
import entities.ValueObjectElement
import org.eclipse.swt.widgets.TreeItem

interface Customization {
    fun addArrayElement(parentItem: TreeItem, a: ArrayElement): Boolean = true
    fun onArrayElementText(item: TreeItem, a: ArrayElement) {}
    fun onArrayElementImage(item: TreeItem) {}

    fun addObjectElement(parentItem: TreeItem, o: ObjectElement): Boolean = true
    fun onObjectElementText(item: TreeItem, o: ObjectElement) {}
    fun onObjectElementImage(item: TreeItem) {}

    fun addValueObjectElement(parentItem: TreeItem, v: ValueObjectElement): Boolean = true
    fun onValueObjectElementText(item: TreeItem, v: ValueObjectElement) {}
    fun onValueObjectElementImage(item: TreeItem) {}

    fun addValueElement(parentItem: TreeItem, v: ValueElement): Boolean = true
    fun onValueElementText(item: TreeItem, v: ValueElement) {}
    fun onValueElementImage(item: TreeItem) {}
}