package gui

import entities.ArrayElement
import entities.ObjectElement
import entities.ValueElement
import entities.ValueObjectElement
import org.eclipse.swt.widgets.TreeItem

class CustomizationImpl : Customization {
    override fun onArrayElementText(item: TreeItem, a: ArrayElement) {
        item.text = "array"
    }

    override fun onObjectElementText(item: TreeItem, o: ObjectElement) {
        item.text = "object"
    }

    override fun onValueObjectElementText(item: TreeItem, v: ValueObjectElement) {
        item.text = v.id
    }

    override fun onValueElementText(item: TreeItem, v: ValueElement) {
        item.text = v.value.toString()
    }

}