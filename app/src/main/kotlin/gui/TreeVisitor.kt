package gui

import entities.ArrayElement
import entities.ObjectElement
import entities.ValueElement
import entities.ValueObjectElement
import entities.Visitor
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Tree
import org.eclipse.swt.widgets.TreeItem

open class TreeVisitor(private val parent: Tree, val custom: Customization = CustomizationImpl()) : Visitor {
    private val currentItem = mutableListOf<TreeItem>()

    override fun visit(o: ObjectElement) {
        if (currentItem.isEmpty()) {
            val item = TreeItem(parent, SWT.NONE)
            custom.onObjectElementText(item, o)
            custom.onObjectElementImage(item)
            item.data = o
            currentItem.add(item)
        } else {
            if (custom.addObjectElement(currentItem.last(), o)) {
                val item = TreeItem(currentItem.last(), SWT.NONE)
                custom.onObjectElementText(item, o)
                custom.onObjectElementImage(item)
                item.data = o
                currentItem.add(item)
            }
        }
    }

    override fun endObjectVisit(o: ObjectElement) {
        if (custom.addObjectElement(currentItem.last(), o))
            currentItem.removeLastOrNull()
    }

    override fun visit(a: ArrayElement) {
        if (currentItem.isEmpty()) {
            val item = TreeItem(parent, SWT.NONE)
            custom.onArrayElementText(item, a)
            custom.onArrayElementImage(item)
            item.data = a
            currentItem.add(item)
        } else {
            if (custom.addArrayElement(currentItem.last(), a)) {
                val item = TreeItem(currentItem.last(), SWT.NONE)
                custom.onArrayElementText(item, a)
                custom.onArrayElementImage(item)
                item.data = a
                currentItem.add(item)
            }
        }
    }

    override fun endArrayVisit(a: ArrayElement) {
        if (custom.addArrayElement(currentItem.last(), a))
            currentItem.removeLastOrNull()
    }

    override fun visit(v: ValueObjectElement) {
        if (currentItem.isEmpty()) {
            val item = TreeItem(parent, SWT.NONE)
            custom.onValueObjectElementText(item, v)
            custom.onValueObjectElementImage(item)
            item.data = v
            currentItem.add(item)
        } else {
            if (custom.addValueObjectElement(currentItem.last(), v)) {
                val item = TreeItem(currentItem.last(), SWT.NONE)
                custom.onValueObjectElementText(item, v)
                custom.onValueObjectElementImage(item)
                item.data = v
                currentItem.add(item)
            }
        }
    }

    override fun endValueObjectElementVisit(v: ValueObjectElement) {
        if (custom.addValueObjectElement(currentItem.last(), v))
            currentItem.removeLastOrNull()
    }

    override fun visit(v: ValueElement) {
        if (currentItem.isEmpty()) {
            val item = TreeItem(parent, SWT.NONE)
            custom.onValueElementText(item, v)
            custom.onValueElementImage(item)
            item.data = v
        } else {
            if (custom.addValueElement(currentItem.last(), v)) {
                val item = TreeItem(currentItem.last(), SWT.NONE)
                custom.onValueElementText(item, v)
                custom.onValueElementImage(item)
                item.data = v
            }
        }
    }
}
