package gui

import entities.Visitor
import org.eclipse.swt.widgets.Tree

interface TreeBuilder {
    fun getVisitor(parent: Tree): Visitor
}