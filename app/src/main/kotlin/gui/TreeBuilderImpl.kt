package gui

import entities.Visitor
import org.eclipse.swt.widgets.Tree

class TreeBuilderImpl: TreeBuilder {
    override fun getVisitor(parent: Tree): Visitor {
        return TreeVisitor(parent, CustomizationImpl())
    }
}