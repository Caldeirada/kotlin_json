package gui

import entities.Visitor
import org.eclipse.swt.widgets.Tree

open class TreeGenerator(val generator: TreeBuilder, val parent: Tree) {
    fun getVisitor(): Visitor{
        return generator.getVisitor(parent)
    }
}