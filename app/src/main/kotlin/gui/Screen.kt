package gui

import actions.Action
import engine.ObjectParser
import entities.BaseElement
import entities.ParseVisitor
import entities.ValueElement
import injector.Inject
import injector.InjectAdd
import org.eclipse.swt.SWT
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.graphics.Color
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.Shell
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.widgets.Tree
import org.eclipse.swt.widgets.TreeItem
import kotlin.reflect.full.isSubclassOf

class Screen {
    val shell: Shell = Shell(Display.getDefault())
    val tree: Tree

    private val text: Text
    private val search: Text
    private val undoList = mutableListOf<Pair<Action, BaseElement>>()

    lateinit var selected: BaseElement
    lateinit var parser: ObjectParser

    @InjectAdd
    private val actions: List<Action> = mutableListOf()

    @Inject
    private lateinit var treeCustomization: Customization

    @Inject
    private lateinit var parseVisitor: ParseVisitor

    init {
        //shell.setSize(500, 500)
        shell.size = Point(500, 500)
        shell.text = "kotlin json"
        shell.layout = GridLayout(2, true)

        tree = Tree(shell, SWT.SINGLE or SWT.BORDER)
        tree.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)
        text = Text(shell, SWT.MULTI or SWT.BORDER or SWT.WRAP or SWT.V_SCROLL)
        text.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)
        text.editable = false

        search = Text(shell, SWT.SINGLE or SWT.MULTI or SWT.BORDER or SWT.WRAP or SWT.V_SCROLL)
        search.layoutData = GridData(SWT.FILL, SWT.FILL, true, false)

        tree.addSelectionListener(object : SelectionAdapter() {
            override fun widgetSelected(e: SelectionEvent) {
                selected = tree.selection.first().data as BaseElement
            }
        })

    }

    private fun fillTree(e: BaseElement) {
        val tv: TreeVisitor
        if (::treeCustomization.isInitialized)
            tv = TreeVisitor(tree, treeCustomization)
        else
            tv = TreeVisitor(tree)
        e.accept(tv)

        tree.data = e
        text.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)
        //get selected element
        tree.addListener(SWT.Selection) { event ->
            if (event.item.data != null) {
                if (event.item.data::class.isSubclassOf(ValueElement::class))
                    text.text = (event.item.data as ValueElement).value.toString()
                else
                    text.text = parser.toString(event.item.data as BaseElement)
            }
        }
    }

    private fun getActions() {
        actions.forEach {
            val button = Button(shell, SWT.PUSH)
            button.text = it.name

            button.addSelectionListener(object : SelectionAdapter() {
                override fun widgetSelected(event: SelectionEvent) {
                    it.execute(event, selected, shell)

                    if (it.addUndo()) {
                        undoList.add(Pair(it, selected))
                    }
                }
            })
        }
    }

    fun open(e: Any) {
        handleParser()
        open(parser.serialize(e))
    }

    private fun handleParser() {
        parser = if (::parseVisitor.isInitialized)
            ObjectParser(parseVisitor)
        else
            ObjectParser()
    }

    fun open(e: BaseElement) {
        handleParser()
        fillTree(e)
        getActions()

        //add undo if any actions is undoable
        if (actions.any { it.addUndo() }) {
            val button = Button(shell, SWT.PUSH)
            button.text = "Undo"

            button.addSelectionListener(object : SelectionAdapter() {
                override fun widgetSelected(event: SelectionEvent) {
                    if (undoList.isNotEmpty()) {
                        val undo = undoList.removeLast()
                        undo.first.undo(undo.second)
                    }
                }
            })
        }

        search.addModifyListener {
            val searchList = if (search.text.isNotEmpty()) parser.search(e, search.text) else emptyList()
            tree.items.forEach {
                searchTree(it!!, searchList)
            }
        }
        shell.open()
        val display = Display.getDefault()
        while (!shell.isDisposed) {
            if (!display.readAndDispatch()) display.sleep()
        }
        display.dispose()
    }

    fun searchTree(treeItem: TreeItem, s: List<BaseElement>) {
        treeItem.items.forEach {
            if (s.contains(it.data))
                it.background = Color(0, 0, 125)
            else
                it.background = null

            if (it.items.isNotEmpty()) {
                searchTree(it, s)
            }
        }
    }
}