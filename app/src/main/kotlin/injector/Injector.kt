package injector

import java.io.File
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.jvm.isAccessible

class Injector {
    companion object {
        val map: MutableMap<String, List<KClass<*>>> = mutableMapOf()

        init {
            val scanner = Scanner(File("app/src/main/resources/application.properties"))
            while (scanner.hasNextLine()) {
                val line = scanner.nextLine()
                val parts = line.split("=")
                map[parts[0]] = parts[1].split(",")
                    .filter { it.isNotEmpty() }
                    .map {
                        Class.forName(it).kotlin
                    }
            }
        }

        fun <T : Any> create(type: KClass<T>): T {
            val instance = type.createInstance()
            type.declaredMemberProperties.forEach {
                it.isAccessible = true
                if (it.hasAnnotation<Inject>()) {
                    val key = type.simpleName + "." + it.name
                    if (map[key] != null && map[key]!!.isNotEmpty()) {
                        val obj = map[key]!!.first().createInstance()
                        (it as KMutableProperty<*>).setter.call(instance, obj)
                    }
                } else if (it.hasAnnotation<InjectAdd>()) {
                    val key = type.simpleName + "." + it.name
                    val obj = map[key]!!.map { it.createInstance() }
                    (it.getter.call(instance) as MutableList<Any>).addAll(obj)
                }
            }

            return instance
        }
    }
}