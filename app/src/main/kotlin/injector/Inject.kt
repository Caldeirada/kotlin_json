package injector

@Target(AnnotationTarget.PROPERTY)
annotation class Inject()

