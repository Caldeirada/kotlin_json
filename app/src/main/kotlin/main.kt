
import engine.ObjectParser
import entities.annotations.Ignore
import entities.annotations.Name
import gui.Screen
import injector.Injector


enum class PType(val cen: String) {
    CENTRO("defe"),
    BALIZA("gk"),
    MARCAGOLOS("ponta")
}

fun main() {
    data class Student(
        @Name("camisola")
        val number: Int,
        val name: String,
        @Ignore
        val graduated: Boolean?,
        val pos: PType
    )

    data class Teacher(
        val name: String,
        val course: String?
    )

    data class ClassRoom(
        @Name("treina-a-dor")
        val teacher: Teacher,
        @Name("nao-correm")
        val students: List<Student>,
        val studentsbyname: Map<String, Student>
    )

    data class ClassRoom2(
        val teacher: Teacher,
        val students: Map<String, Student>
    )

    data class School(
        val name: String,
        val room: ClassRoom
    )

    val adan = Student(1, "Adan", false, PType.BALIZA)
    val coates = Student(4, "Coates", false, PType.MARCAGOLOS)
    val santos = Student(11, "Nuno Santos", false, PType.CENTRO)
    val amorim = Teacher("Amorin", "Campeao")
    val mapa = mapOf("cenas" to adan, "cotes" to coates)
    val scp = ClassRoom(amorim, listOf(adan, coates, santos), mapa)

    val alvalade = School("alvalade", scp)

    val ls = listOf("a", listOf(1, 2, 4, 4))
    //val jv0 = JsonVisitor()

    //val obj4 = parse(mapa)

    val cs2 = ClassRoom2(amorim, mapa)
    //val obj5 = parse(cs2)

    val p = ObjectParser()
    //val obj0 = parse(ls)
    //obj0.accept(jv0)
    //println(p.stringify(ls))
    //val obj = parse(adan)
    //val obj2 = parse(scp)
    //val obj3 = parse(alvalade)
    //val jv = JsonVisitor()

    val cenas = mapOf<String, String>()
    //println(p.stringify(cenas))
    //obj3.accept(jv)
    println(p.stringify(scp))
    //println(p.stringify(alvalade))

    //println(p.search( scp, "name"))
    //println(p.search(scp,"name", "pos"))
    val w = Injector.create(Screen::class)
    //FileTreeSkeleton().open(p.serialize(scp))
    //w.open(p.serialize(scp))
    w.open(scp)
}



/*class FileTreeSkeleton() {
    private val shell: Shell = Shell(Display.getDefault())
    val tree: Tree
    val text: Text
    private val search: Text
    private lateinit var selected: BaseElement

    @InjectAdd
    private val actions: List<Action> = mutableListOf()

    init {


        shell.setSize(500, 500)
        shell.text = "kotlin json"
        shell.layout = GridLayout(2, true)

        tree = Tree(shell, SWT.SINGLE or SWT.BORDER)
        tree.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)
        text = Text(shell, SWT.MULTI or SWT.BORDER or SWT.WRAP or SWT.V_SCROLL)
        text.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)
        text.editable = false

        search = Text(shell, SWT.SINGLE or SWT.MULTI or SWT.BORDER or SWT.WRAP or SWT.V_SCROLL)
        search.layoutData = GridData(SWT.FILL, SWT.FILL, true, false)

        tree.addSelectionListener(object : SelectionAdapter() {
            override fun widgetSelected(e: SelectionEvent) {
                selected = tree.selection.first().data as BaseElement
                println("selected: " + tree.selection.first().data)
            }
        })

    }

    fun fillTree(folder: BaseElement) {

        val searcher = object : Visitor {
            val parentList = mutableListOf<TreeItem>()
            var addToCurrent = false
            lateinit var currentItem: TreeItem

            override fun visit(o: ObjectElement) {
                if (parentList.isEmpty()) {
                    val item = TreeItem(tree, SWT.NONE)
                    item.text = "object"
                    item.data = o
                    parentList.add(item)
                } else {
                    if (addToCurrent) {
                        parentList.add(currentItem)
                    } else {
                        val item = TreeItem(parentList.last(), SWT.NONE)
                        item.text = "item"
                        item.data = o
                        parentList.add(item)
                    }
                }
            }

            override fun endObjectVisit(o: ObjectElement) {
                parentList.removeLastOrNull()
                addToCurrent = false
            }

            override fun visit(a: ArrayElement) {
                if (parentList.isEmpty()) {
                    val item = TreeItem(tree, SWT.NONE)
                    item.text = "array"
                    item.data = a
                    parentList.add(item)
                } else {
                    if (addToCurrent) {
                        parentList.add(currentItem)
                        addToCurrent = false
                    } else {
                        val item = TreeItem(parentList.last(), SWT.NONE)
                        item.text = "array"
                        item.data = a
                        parentList.add(item)
                    }
                }
            }

            override fun endArrayVisit(a: ArrayElement) {
                parentList.removeLastOrNull()
                addToCurrent = false
            }

            override fun visit(v: ValueObjectElement) {
                if (parentList.isEmpty()) {
                    val item = TreeItem(tree, SWT.NONE)
                    item.text = v.id
                    item.data = v
                    addToCurrent = true
                    currentItem = item
                } else {
                    val item = TreeItem(parentList.last(), SWT.NONE)
                    item.text = v.id
                    item.data = v
                    addToCurrent = true
                    currentItem = item
                }
            }

            override fun visit(v: ValueElement) {
                currentItem.text = "${currentItem.text} : ${v.value.toString()}"
            }
        }

        folder.accept(searcher)
    }


    fun open(e: BaseElement) {
        fillTree(e)

        actions.forEach {
            val button = Button(shell, SWT.PUSH)
            button.text = it.name

            button.addSelectionListener(object : SelectionListener {
                override fun widgetSelected(event: SelectionEvent) {
                    it.execute(event)
                }

                override fun widgetDefaultSelected(p0: SelectionEvent?) {
                    TODO("Not yet implemented")
                }
            })
        }

        tree.data = e
        val o = ObjectParser()
        text.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)
        //tree.expandAll()
        shell.pack()
        shell.open()
        tree.addListener(SWT.Selection, object : Listener {
            override fun handleEvent(event: Event) {
                println("click")

                if (event.item.data != null) {
                    text.text = o.toString(event.item.data as BaseElement)
                }
            }
        })

        search.addModifyListener {
            println(search.text)
            val s = if (search.text.isNotEmpty()) o.search(e, search.text) else emptyList()
            println(s)
            tree.items.forEach {
                searchTree(it!!, s)
            }
        }
        val display = Display.getDefault()
        while (!shell.isDisposed) {
            if (!display.readAndDispatch()) display.sleep()
        }
        display.dispose()
    }

    private fun searchTree(treeItem: TreeItem, s: List<BaseElement>) {
        treeItem.items.forEach {
            if (s.contains(it.data))
                it.background = Color(0, 0, 125)
            else
                it.background = null

            if (it.items.isNotEmpty()) {
                searchTree(it, s)
            }
        }
    }
}*/