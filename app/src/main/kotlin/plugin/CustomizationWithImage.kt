package plugin

import entities.ArrayElement
import entities.ObjectElement
import entities.ValueElement
import entities.ValueObjectElement
import gui.Customization
import org.eclipse.swt.graphics.Image
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.TreeItem

class CustomizationWithImage : Customization {
    override fun onArrayElementText(item: TreeItem, a: ArrayElement) {
        item.text = "array"
    }

    override fun onArrayElementImage(a: TreeItem) {
        a.image = Image(Display.getDefault(), "app/src/main/resources/images/archive.svg")
    }

    override fun addObjectElement(parentItem: TreeItem, o: ObjectElement): Boolean {
        return false /*parentItem.data::class.isSubclassOf(ArrayElement::class) ||
                (parentItem.parentItem::class.isSubclassOf(ArrayElement::class))*/
    }

    override fun addValueElement(parentItem: TreeItem, v: ValueElement): Boolean {
        return false
    }

    override fun addArrayElement(parentItem: TreeItem, a: ArrayElement): Boolean {
        return false
    }

    override fun onObjectElementText(item: TreeItem, o: ObjectElement) {
        item.text = "object"
    }

    override fun onObjectElementImage(o: TreeItem) {
        o.image = Image(Display.getDefault(), "app/src/main/resources/images/book.svg")
    }

    override fun onValueObjectElementText(item: TreeItem, v: ValueObjectElement) {
        item.text = v.id
    }

    override fun onValueObjectElementImage(v: TreeItem) {
        v.image = Image(Display.getDefault(), "app/src/main/resources/images/book-open.svg")
    }

}
