package actions

import entities.BaseElement
import entities.ValueElement
import entities.ValueObjectElement
import org.eclipse.swt.SWT
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.Shell
import org.eclipse.swt.widgets.Text
import kotlin.reflect.full.isSubclassOf


class Change : Action {
    override val name: String = "Change"
    private var prevValue = mutableListOf<ValueElement>()

    override fun execute(event: SelectionEvent, selection: BaseElement, shell: Shell) {
        if (selection::class.isSubclassOf(ValueElement::class)) {
            changeValue(selection as ValueElement)
        }

        if (selection::class.isSubclassOf(ValueObjectElement::class) &&
            (selection as ValueObjectElement).value::class.isSubclassOf(ValueElement::class)
        ) {
            changeValue(selection.value as ValueElement)
        }
    }

    private fun changeValue(e: ValueElement) {
        prevValue.add(ValueElement(e.value))
        val child = Shell(Display.getDefault())
        child.layout = GridLayout(1, true)
        child.setSize(200, 200)
        val text = Text(child, SWT.MULTI or SWT.BORDER or SWT.WRAP)
        text.text = e.value.toString()
        text.layoutData = GridData(SWT.FILL, SWT.FILL, true, true)

        val saveButton = Button(child, SWT.PUSH)
        saveButton.text = "Save"

        val cancelButton = Button(child, SWT.PUSH)
        cancelButton.text = "Cancel"


        saveButton.addSelectionListener(object : SelectionAdapter() {
            override fun widgetSelected(event: SelectionEvent) {
                e.value = text.text
                child.close()
            }
        })

        cancelButton.addSelectionListener(object : SelectionAdapter() {
            override fun widgetSelected(event: SelectionEvent) {
                child.dispose()
            }
        })

        child.open()
    }

    override fun addUndo(): Boolean = true

    override fun undo(selection: BaseElement) {
        if (selection::class.isSubclassOf(ValueElement::class)) {
            (selection as ValueElement).value = prevValue.removeLast().value
        } else if (selection::class.isSubclassOf(ValueObjectElement::class) &&
            (selection as ValueObjectElement).value::class.isSubclassOf(ValueElement::class)
        ) {
            (selection.value as ValueElement).value = prevValue.removeLast().value
        } else
            prevValue.removeLastOrNull()
    }
}