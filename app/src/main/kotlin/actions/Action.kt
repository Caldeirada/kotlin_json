package actions

import entities.BaseElement
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.widgets.Shell

interface Action {
    val name: String
    fun execute(event: SelectionEvent, selection: BaseElement, shell: Shell)

    fun addUndo(): Boolean = false
    fun undo(selection: BaseElement) {}
}
