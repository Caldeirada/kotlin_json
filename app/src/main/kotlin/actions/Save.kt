package actions

import engine.ObjectParser
import entities.BaseElement
import org.eclipse.swt.SWT
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.widgets.FileDialog
import org.eclipse.swt.widgets.Shell
import java.io.File

class Save : Action {
    override val name: String = "Save"
    override fun execute(event: SelectionEvent, selection: BaseElement, shell: Shell) {
        val dialog = FileDialog(shell, SWT.SAVE)
        dialog.fileName = "element.txt"
        val fileName = dialog.open()
        val o = ObjectParser()

        if (fileName != null)
            File(fileName).writeText(o.toString(selection))
    }
}