package actions

import entities.BaseElement
import gui.Screen
import injector.Injector
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.widgets.Shell

class NewWindow: Action {
    override val name: String = "Open"

    override fun execute(event: SelectionEvent, selection: BaseElement, shell: Shell) {
        val s = Injector.create(Screen::class)

        s.open(selection)
    }
}