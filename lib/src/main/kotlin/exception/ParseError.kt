package exception

class ParseError(message: String) : Exception(message)