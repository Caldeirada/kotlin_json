package common

internal const val LINE_BREAK = "\n"
internal const val EMPTY_STRING = ""

//json characters
internal const val LEFT_BRACE = "{"
internal const val RIGHT_BRACE = "}"
internal const val LEFT_BRACKET = "["
internal const val RIGHT_BRACKET = "]"
internal const val WHITESPACE = " " // add space, line feed, carriage return, horizontal tab
internal const val COMMA = ","
internal const val COLON = ":"
internal const val DOUBLE_QUOTES = "\""
