package engine

import common.COLON
import common.COMMA
import common.DOUBLE_QUOTES
import common.EMPTY_STRING
import common.LEFT_BRACE
import common.LEFT_BRACKET
import common.LINE_BREAK
import common.RIGHT_BRACE
import common.RIGHT_BRACKET
import common.WHITESPACE
import entities.ArrayElement
import entities.ObjectElement
import entities.ParseVisitor
import entities.ValueElement
import entities.ValueObjectElement
import kotlin.reflect.full.isSubclassOf

internal class JsonVisitor : ParseVisitor {
    val identent: Int = 4
    private var jsonStringify: String = EMPTY_STRING

    //flag list to indicate current structure being handled, array or object
    private var isArrayFlagList: MutableList<Boolean> = mutableListOf()
    private var jsonEntities: MutableList<MutableList<String>> = mutableListOf()
    private var indentation: MutableList<String> = mutableListOf()

    override fun getStringified(): String {
        return if (jsonEntities.isEmpty())
            jsonStringify
        else {
            val l = jsonEntities.removeLast()
            getEntityString(l)
        }
    }

    override fun visit(a: ArrayElement) {
        indentation.addAll((0..identent).map { WHITESPACE })
        isArrayFlagList.add(true)
        jsonEntities.add(mutableListOf())
    }

    override fun endArrayVisit(a: ArrayElement) {
        isArrayFlagList.removeLast()
        removeIdentation()
        val entity = jsonEntities.removeLast()
        if (jsonEntities.isNotEmpty())
            if (isArrayFlagList.last())
                jsonEntities.last().add(indentation.joinToString(separator = EMPTY_STRING) + getArrayEntity(entity))
            else
                jsonEntities.last()[jsonEntities.last().lastIndex] += getArrayEntity(entity)
        else
            jsonStringify = getArrayEntity(entity)

    }

    private fun getArrayEntity(entity: MutableList<String>) =
        getEntityString(entity, separator = COMMA, prefix = LEFT_BRACKET, postfix = RIGHT_BRACKET)

    override fun visit(o: ObjectElement) {
        indentation.addAll((0..identent).map { WHITESPACE })
        isArrayFlagList.add(false)
        jsonEntities.add(mutableListOf())
    }

    override fun endObjectVisit(o: ObjectElement) {
        isArrayFlagList.removeLast()
        removeIdentation()
        val entity = jsonEntities.removeLast()
        if (jsonEntities.isNotEmpty())
            if (isArrayFlagList.last() || jsonEntities.last().isEmpty())
                jsonEntities.last().add(indentation.joinToString(separator = EMPTY_STRING) + getEntityString(entity))
            else
                jsonEntities.last()[jsonEntities.last().lastIndex] += getEntityString(entity)
        else
            jsonStringify = getEntityString(entity)

    }

    private fun getEntityString(
        entity: MutableList<String>,
        separator: String = COMMA,
        prefix: String = LEFT_BRACE,
        postfix: String = RIGHT_BRACE
    ) =
        entity.joinToString(
            separator = separator + LINE_BREAK,
            prefix = prefix + LINE_BREAK,
            postfix = LINE_BREAK + indentation.joinToString(separator = EMPTY_STRING) + postfix
        )

    override fun visit(v: ValueElement) {
        //if is array or first json element
        if (jsonEntities.last().isEmpty() || isArrayFlagList.last())
            jsonEntities.last().add(indentation.joinToString(separator = EMPTY_STRING) + getValue(v.value))
        else
            jsonEntities.last()[jsonEntities.last().lastIndex] += (getValue(v.value))
    }

    private fun getValue(value: Any?): String {
        return if (value != null && value::class.isSubclassOf(String::class))
            DOUBLE_QUOTES + value + DOUBLE_QUOTES
        else
            value.toString()
    }


    override fun visit(v: ValueObjectElement) {
        indentation.addAll((0..identent).map { WHITESPACE })
        if (jsonEntities.isEmpty()) {
            isArrayFlagList.add(false)
            jsonEntities.add(mutableListOf(indentation.joinToString(separator = EMPTY_STRING) + DOUBLE_QUOTES + v.id + DOUBLE_QUOTES + WHITESPACE + COLON + WHITESPACE))
        } else {
            jsonEntities.last()
                .add(indentation.joinToString(separator = EMPTY_STRING) + DOUBLE_QUOTES + v.id + DOUBLE_QUOTES + WHITESPACE + COLON + WHITESPACE)
        }
    }

    override fun endValueObjectElementVisit(v: ValueObjectElement) {
        removeIdentation()
    }

    private fun removeIdentation() {
        (0..identent).forEach { _ -> indentation.removeLastOrNull() }
    }
}
