package engine

import entities.BaseElement
import entities.ValueElement
import entities.ValueObjectElement
import entities.Visitor
import kotlin.reflect.full.isSubclassOf

class SearchVisitor(var searchList: List<String>) : Visitor {
    val searchResults: MutableList<BaseElement> = mutableListOf()

    override fun visit(v: ValueObjectElement) {
        if (searchList.any { v.id.toLowerCase().contains(it.toLowerCase()) })
            searchResults.add(v)
        else if(v.value::class.isSubclassOf(ValueElement::class))
            if (searchList.any { (v.value as ValueElement).value.toString().toLowerCase().contains(it.toLowerCase()) })
                searchResults.add(v)
    }

}