package engine

import common.UNSUPPORTED_TYPE_ERROR
import entities.ArrayElement
import entities.BaseElement
import entities.ObjectElement
import entities.ValueElement
import entities.ValueObjectElement
import entities.annotations.Ignore
import entities.annotations.Name
import exception.ParseError
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.jvm.jvmErasure

internal class Parser() {

    companion object {
        private val p: Parser = Parser()
        fun parse(o: Any): BaseElement {
            val clazz: KClass<Any> = o::class as KClass<Any>

            return if (clazz.isSubclassOf(Collection::class))
                ArrayElement((o as List<Any>).map { p.handleArray(it) })
            else if (clazz.isSubclassOf(Map::class))
                ObjectElement(p.getMapElements(o as Map<String, Any>))
            else
                ObjectElement(p.getElements(clazz, o))
        }
    }

        private fun getMapElements(o: Map<String, Any>): List<ValueObjectElement> {
            return o.keys.map { ValueObjectElement(it, extractMapElement(o[it]!!)) }
        }

        private fun getElements(clazz: KClass<*>, o: Any?): List<ValueObjectElement> {
            return clazz.declaredMemberProperties
                .filter { handleIgnore(it) }
                .map {
                    ValueObjectElement(getName(it), extractElement(it, o))
                }
        }

        private fun handleIgnore(it: KProperty1<out Any, *>) = !(it.hasAnnotation<Ignore>())

        private fun getName(property: KProperty1<out Any, *>): String {
            return if (property.hasAnnotation<Name>())
                property.findAnnotation<Name>()!!.name
            else
                property.name
        }

        private fun extractMapElement(o: Any): BaseElement {
            val clazz: KClass<Any> = o::class as KClass<Any>

            return if (clazz.isSubclassOf(Number::class) || clazz.isSubclassOf(Boolean::class) || clazz.isSubclassOf(
                    String::class
                )
            )
                ValueElement(o)
            else if (clazz.isData)
                ObjectElement(getElements(clazz, o))
            else if (clazz.isSubclassOf(Collection::class))
                ArrayElement((o as List<Any>).map { handleArray(it) })
            else if (clazz.isSubclassOf(Array::class))
                ArrayElement(listOf(o).map { handleArray(it) })
            else ObjectElement(getElements(clazz, o))
        }

        private fun extractElement(element: KProperty1<out Any, *>, o: Any?): BaseElement {
            val clazz = element.returnType.jvmErasure

            return if (clazz.isSubclassOf(Number::class) || clazz.isSubclassOf(Boolean::class) || clazz.isSubclassOf(
                    String::class
                )
            )
                ValueElement(element.call(o))
            else if (clazz.isData)
                ObjectElement(getElements(clazz, element.call(o)))
            else if (clazz.isSubclassOf(Collection::class))
                ArrayElement((element.call(o) as List<Any>).map { handleArray(it) })
            else if (clazz.isSubclassOf(Array::class))
                ArrayElement(listOf(o as Any).map { handleArray(it) })
            else if (clazz.isSubclassOf(Map::class))
                ObjectElement(getMapElements(element.call(o) as Map<String, Any>))
            else if (clazz.isSubclassOf(Enum::class)) {
                ValueElement(element.call(o).toString())
            } else ObjectElement(getElements(clazz, element.call(o)))
        }

        private fun handleArray(element: Any): BaseElement {
            val clazz = element::class as KClass<Any>
            return if (clazz.isData)
                parse(element)
            else if (clazz.isSubclassOf(Number::class) || clazz.isSubclassOf(Boolean::class) || clazz.isSubclassOf(
                    String::class
                )
            )
                ValueElement(element)
            else if (clazz.isSubclassOf(Collection::class))
                parse(element)
            else
                throw ParseError(UNSUPPORTED_TYPE_ERROR)
        }
}