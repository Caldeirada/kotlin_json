package engine

import engine.Parser.Companion.parse
import entities.ArrayElement
import entities.BaseElement
import entities.ParseVisitor

class ObjectParser(private val v: ParseVisitor = JsonVisitor()) {
    fun stringify(o: Any): String {
        return toString(parse(o))
    }

    fun toString(o: BaseElement): String{
        o.accept(v)
        return v.getStringified()
    }

    fun searchToString(o: Any, vararg search: String): List<String> {
        return search(parse(o), *search)
            .map { ArrayElement(listOf(it)) }
            .map {
                it.accept(v)
                v.getStringified()
            }
    }

    fun search(element: BaseElement, vararg search: String): List<BaseElement> {
        val searchVisitor = SearchVisitor(search.toList())

        element.accept(searchVisitor)

        return searchVisitor.searchResults
    }

    fun serialize(o: Any): BaseElement = parse(o)
}