package entities

class ValueObjectElement(val id: String, val value: BaseElement) : BaseElement() {
    override fun accept(v: Visitor) {
        v.visit(this)
        value.accept(v)
        v.endValueObjectElementVisit(this)
    }
}
