package entities.annotations

@Target(AnnotationTarget.PROPERTY)
annotation class Name(val name: String)
