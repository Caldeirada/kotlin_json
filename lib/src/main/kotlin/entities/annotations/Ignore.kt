package entities.annotations

@Target(AnnotationTarget.PROPERTY)
annotation class Ignore