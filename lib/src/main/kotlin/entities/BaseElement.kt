package entities

abstract class BaseElement {
    abstract fun accept(v: Visitor)
}