package entities

class ObjectElement(val values: List<ValueObjectElement>) : BaseElement() {
    override fun accept(v: Visitor) {
        v.visit(this)
        values.forEach {
            it.accept(v)
        }
        v.endObjectVisit(this)
    }
}