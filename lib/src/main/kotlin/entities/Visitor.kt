package entities

interface Visitor {
    fun visit(b: BaseElement): Boolean = true
    fun visit(o: ObjectElement) {}
    fun endObjectVisit(o: ObjectElement) {}
    fun visit(v: ValueElement) {}
    fun visit(a: ArrayElement) {}
    fun endArrayVisit(a: ArrayElement) {}
    fun visit(v: ValueObjectElement) {}
    fun endValueObjectElementVisit(v: ValueObjectElement) {}
}