package entities

class ValueElement(var value: Any?) : BaseElement() {
    override fun accept(v: Visitor) {
        v.visit(this)
    }
}
