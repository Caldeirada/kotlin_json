package entities

interface ParseVisitor : Visitor {
    fun getStringified(): String
}