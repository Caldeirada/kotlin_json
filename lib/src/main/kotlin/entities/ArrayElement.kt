package entities

class ArrayElement(val values: List<BaseElement>) : BaseElement() {
    override fun accept(v: Visitor) {
        v.visit(this)
        values.forEach {
            it.accept(v)
        }
        v.endArrayVisit(this)
    }
}