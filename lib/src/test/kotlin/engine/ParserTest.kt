package engine

import entities.ArrayElement
import entities.ObjectElement
import entities.ValueElement
import entities.ValueObjectElement
import entities.annotations.Ignore
import entities.annotations.Name
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

data class P5Character(
    val name: String,
    val persona: String
)

data class P5Party(
    val party: List<P5Character>
)

data class Fridge(
    val full: Boolean,
    @Name("to_eat")
    val fruits: List<String>,
    @Ignore
    val brand: String
)

class ParserTest {

    @Test
    fun given_simple_dataclass_expect_serialized_element() {
        val fs = P5Character("Futaba Sakura", "Necronomicon")
        val serialized = Parser.parse(fs)

        assertNotNull(serialized)
        assertEquals(2, (serialized as ObjectElement).values.size)
        assertTrue(serialized.values.map { ((it as ValueObjectElement).value as ValueElement).value }
            .containsAll(listOf("Futaba Sakura", "Necronomicon")))
    }

    @Test
    fun given_dataclass_with_ignore_annotation_expect_correct_ignored() {
        val fridge = Fridge(false, listOf("apples", "peach", "tomatoes"), "jvm-kotlin")
        val serialized = Parser.parse(fridge)

        assertNotNull(serialized)
        assertEquals(2, (serialized as ObjectElement).values.size)
        assertFalse(serialized.values.map { (it as ValueObjectElement).id }.contains("brand"))
    }

    @Test
    fun given_dataclass_with_name_annotation_expect_name_changed() {
        val fridge = Fridge(false, listOf("apples", "peach", "tomatoes"), "jvm-kotlin")
        val serialized = Parser.parse(fridge)

        assertNotNull(serialized)
        assertEquals(2, (serialized as ObjectElement).values.size)
        assertTrue(serialized.values.map { (it as ValueObjectElement).id }.contains("to_eat"))
    }

    @Test
    fun given_simple_dataclass_with_list_expect_parse_with_list() {
        val mc = P5Character("MC", "WildCard")
        val fs = P5Character("Futaba Sakura", "Necronomicon")
        val party = P5Party(party = listOf(mc, fs))
        val serialized = Parser.parse(party)

        assertNotNull(serialized)
        assertEquals(1, (serialized as ObjectElement).values.size)
        assertEquals(ArrayElement::class, (serialized.values.first() as ValueObjectElement).value::class)
    }
}
