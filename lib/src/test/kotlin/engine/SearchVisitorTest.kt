package engine

import entities.ValueElement
import entities.ValueObjectElement
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SearchVisitorTest {

    @Test
    fun given_dataclass_expect_search_yield_results() {
        val mc = P5Character("MC", "WildCard")
        val fs = P5Character("Futaba Sakura", "Necronomicon")
        val party = P5Party(party = listOf(mc, fs))
        val serialized = Parser.parse(party)
        val search = SearchVisitor(listOf("persona"))

        serialized.accept(search)

        assertTrue(search.searchResults.isNotEmpty())
        assertEquals(2, search.searchResults.map { (it as ValueObjectElement).value }.size)
        assertTrue(search.searchResults.map { ((it as ValueObjectElement).value as ValueElement).value }
            .containsAll(listOf("WildCard", "Necronomicon")))
        assertTrue(search.searchResults.map { (it as ValueObjectElement).id }.contains("persona"))
    }

    @Test
    fun given_dataclass_expect_search_yield_no_results() {
        val mc = P5Character("MC", "WildCard")
        val fs = P5Character("Futaba Sakura", "Necronomicon")
        val party = P5Party(party = listOf(mc, fs))
        val serialized = Parser.parse(party)
        val search = SearchVisitor(listOf("skill_card"))

        serialized.accept(search)

        assertTrue(search.searchResults.isEmpty())
    }
}