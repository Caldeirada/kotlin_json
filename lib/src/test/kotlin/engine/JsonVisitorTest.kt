package engine

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class JsonVisitorTest {

    @Test
    fun given_simple_dataclass_expect_correct_json_output() {
        val fb = P5Character("Futaba Sakura", "Necronomicon")
        val serialized = Parser.parse(fb)
        val jv = JsonVisitor()

        serialized.accept(jv)

        assertNotNull(jv.getStringified())
        assertEquals(
            "{\n" +
                    "          \"name\" : \"Futaba Sakura\",\n" +
                    "          \"persona\" : \"Necronomicon\"\n" +
                    "}", jv.getStringified()
        )
    }

    @Test
    fun given_array_expect_json_list(){
        val l = listOf(1, 2, 4, 4)
        val serialized = Parser.parse(l)
        val jv = JsonVisitor()

        serialized.accept(jv)

        assertNotNull(jv.getStringified())
        assertEquals(
            "[\n" +
                    "     1,\n" +
                    "     2,\n" +
                    "     4,\n" +
                    "     4\n" +
                    "]", jv.getStringified()
        )
    }
}